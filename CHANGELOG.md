# Changelog

## v1.2.0 - 10/06/2024
### Added
+ Add Emmaitar support (show author and title of a painting)
+ Armor stands now show the armor stand item texture
### Fixed
+ Fix some lotr leaves, beams, armor stand name
+ Ensure the fixed names work with enhanced tooltips

## v1.1.0 - 25/05/2024
### Added
+ Fix missing language keys for all known lotr blocks
+ Use item icons for beds, plates and grapevines instead of block version
### Fixed
+ Fix player uuid name retrieval for banners

## v1.0.0 - 08/12/2019
### Added
+ Added warg support
+ Added spider support
+ Added hired unit support
+ Added banner support
