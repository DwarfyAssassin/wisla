package io.gitlab.dwarfyassassin.wisla.util;

import mcp.mobius.waila.api.SpecialChars;
import net.minecraft.client.Minecraft;
import net.minecraft.util.StatCollector;

import java.util.List;

public class GeneralUtils {

    /**
     * Translates the input string, preserving the enhanced tooltip info
     */
    public static List<String> translateUtil(List<String> tip, String newInput) {
        var translation = SpecialChars.WHITE + StatCollector.translateToLocal(newInput);
        if (Minecraft.getMinecraft().gameSettings.advancedItemTooltips) {
            var splitString = tip.get(0).split(" \\(#");
            if (splitString.length == 2) {
                tip.set(0, translation + " (#" + splitString[1]);
            } else if (splitString.length == 1) {
                tip.set(0, translation);
            }
        } else {
            tip.set(0, translation);
        }
        return tip;
    }
}
