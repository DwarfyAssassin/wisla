package io.gitlab.dwarfyassassin.wisla;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import io.gitlab.dwarfyassassin.wisla.addon.emmaitar.AddonEmmaitar;
import io.gitlab.dwarfyassassin.wisla.addon.lotr.AddonLOTRBlocks;
import io.gitlab.dwarfyassassin.wisla.addon.lotr.AddonLOTREntities;
import io.gitlab.dwarfyassassin.wisla.addon.lotr.AddonLOTRNPC;
import io.gitlab.dwarfyassassin.wisla.addon.wawla.AddonWawlaLOTREntities;

@Mod(modid=ModConstants.ID, name=ModConstants.NAME, version= VersionTag.VERSION, acceptableRemoteVersions="*", dependencies="required-after:Waila;required-after:lotr;after:wawla")
public class Wisla {

    public static boolean isWawlaLoaded;
    public static boolean isEmmaitarLoaded;

    @EventHandler
    @SuppressWarnings("unused")
    public void init(FMLInitializationEvent event) {
        isWawlaLoaded = Loader.isModLoaded("wawla");
        isEmmaitarLoaded = Loader.isModLoaded("emmaitar");

        FMLInterModComms.sendMessage("Waila", "register", AddonLOTREntities.class.getName() + ".registerAddon");
        FMLInterModComms.sendMessage("Waila", "register", AddonLOTRNPC.class.getName() + ".registerAddon");
        FMLInterModComms.sendMessage("Waila", "register", AddonLOTRBlocks.class.getName() + ".registerAddon");
        if(isWawlaLoaded) FMLInterModComms.sendMessage("Waila", "register", AddonWawlaLOTREntities.class.getName() + ".registerAddon");
        if(isEmmaitarLoaded) FMLInterModComms.sendMessage("Waila", "register", AddonEmmaitar.class.getName() + ".registerAddon");
    }

}
