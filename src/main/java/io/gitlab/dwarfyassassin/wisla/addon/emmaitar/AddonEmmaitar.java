package io.gitlab.dwarfyassassin.wisla.addon.emmaitar;

import emmaitar.common.EntityCustomPainting;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaEntityAccessor;
import mcp.mobius.waila.api.IWailaEntityProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import java.util.List;

import static io.gitlab.dwarfyassassin.wisla.util.GeneralUtils.translateUtil;

public class AddonEmmaitar implements IWailaEntityProvider {

    @Override
    public Entity getWailaOverride(IWailaEntityAccessor accessor, IWailaConfigHandler config) {
        return accessor.getEntity();
    }

    @Override
    public List<String> getWailaHead(Entity entity, List<String> tip, IWailaEntityAccessor accessor, IWailaConfigHandler config) {
        if (entity instanceof EntityCustomPainting) {
            translateUtil(tip, "item.painting.name");
        }
        return tip;
    }

    @Override
    public List<String> getWailaBody(Entity entity, List<String> tip, IWailaEntityAccessor accessor, IWailaConfigHandler config) {
        if (entity instanceof EntityCustomPainting entityCustomPainting) {
            var customPaintingData = entityCustomPainting.getCustomPaintingData();
            tip.add(StatCollector.translateToLocal("tooltip.wisla.emmaitar.author") + ": " + customPaintingData.authorName);
            tip.add(StatCollector.translateToLocal("tooltip.wisla.emmaitar.title") + ": " + customPaintingData.title);
        }
        return tip;
    }

    @Override
    public List<String> getWailaTail(Entity entity, List<String> tip, IWailaEntityAccessor accessor, IWailaConfigHandler config) {
        return tip;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, Entity ent, NBTTagCompound tag, World world) {
        return tag;
    }

    @SuppressWarnings("unused")
    public static void registerAddon(IWailaRegistrar register) {
        AddonEmmaitar dataProvider = new AddonEmmaitar();

        register.registerHeadProvider(dataProvider, EntityCustomPainting.class);
        register.registerBodyProvider(dataProvider, EntityCustomPainting.class);
    }
}
